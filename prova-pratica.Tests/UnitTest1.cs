using NUnit.Framework;
using ProvaPratica;

namespace prova_pratica.Tests
{
    public class Tests
    {
       
        [Test]
        public void TesteDeCPF()
        {   
            string documento = "44004373026";
            bool result = true;
            Assert.AreEqual(result, Validador.CPF(documento));
        }

        [Test]
         public void TesteDeCNPJ()
        {   
            string documento = "35738318000159";
            bool result = true;
            Assert.AreEqual(result, Validador.CNPJ(documento));
        }

        public void TesteDeCPFfalse()
        {   
            string documento = "440043730261";
            bool result = true;
            Assert.AreEqual(result, Validador.CPF(documento));
        }

        [Test]
         public void TesteDeCNPJfalse()
        {   
            string documento = "357383180001591";
            bool result = true;
            Assert.AreEqual(result, Validador.CNPJ(documento));
        }

        

    }
}